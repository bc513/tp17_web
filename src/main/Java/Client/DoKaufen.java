package Client;

import Code.User;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class DoKaufen implements ClientData{
    private int[] id;
    private int credit;
    private User user;

    public DoKaufen(int[] id, int credit, User user) throws Exception {
        this.id = id;
        this.credit = credit;
        this.user = user;
    }
    private final Logger logger = LoggerFactory.getLogger(getClass());
    BrowseUser browseUser = new BrowseUser("Credit");
    BrowseUser browseUser2 = new BrowseUser("RecGekauft");
    Map<String, NodeId>  map = browseUser.getName_pass_map();
    Map<String, NodeId>  map2 = browseUser2.getName_pass_map();


    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        NodeId nodeId = map.get(user.getUsername());
        NodeId nodeId1 = map2.get(user.getUsername());


        nodeId.getIdentifier();



        Variant v = new Variant(credit);
        Variant v2 = new Variant(id);


        DataValue dv = new DataValue(v,null,null);
        DataValue dv2 = new DataValue(v2,null,null);

        // write asynchronously....
        CompletableFuture<StatusCode> f =
                client.writeValue(nodeId, dv);
        CompletableFuture<StatusCode> f2 =
                client.writeValue(nodeId1, dv2);


        // ...but block for the results so we write in order
        StatusCode statusCodes = f.get();
        StatusCode statusCodes2 = f2.get();
        if (statusCodes.isGood()) {
            logger.info("Wrote '{}' to nodeId={} statusCode = {}", v, nodeId, statusCodes);
            logger.info("Wrote '{}' to nodeId={} statusCode = {}", v, nodeId1, statusCodes2);
        }

        future.complete(client);
    }
}
