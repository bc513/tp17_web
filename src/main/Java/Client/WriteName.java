package Client;

import Code.User;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.soap.Node;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class WriteName implements ClientData{
    private User user;
    private NodeId nodeId;
    BrowseUser browseUser = new BrowseUser("Name");
    Map<String,NodeId> map=browseUser.getName_pass_map();

    public WriteName(User user) throws Exception {
        this.user = user;

    }
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        NodeId nodeId = map.get(user.getUsername());
        nodeId.getIdentifier();


        Variant v = new Variant(user.getName());
        DataValue dv = new DataValue(v,null,null);
        // write asynchronously....
        CompletableFuture<StatusCode> f =
                client.writeValue(nodeId, dv);


        // ...but block for the results so we write in order
        StatusCode statusCodes = f.get();
        if (statusCodes.isGood()) {
            logger.info("Wrote '{}' to nodeId={} statusCode = {}", v, nodeId, statusCodes);
        }

        future.complete(client);
    }
}
