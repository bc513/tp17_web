package Code;

public class HochVoltBatterie {

    private int gewicht;		//## attribute gewicht

    private String produktionsJahr;		//## attribute produktionsJahr

    private String stateOfHealth;		//## attribute stateOfHealth


    private int credit=10;

    private int hochvoltBatterie_id = -1;

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getHochvoltBatterie_id() {
        return hochvoltBatterie_id;
    }

    public void setHochvoltBatterie_id(int hochvoltBatterie_id) {
        this.hochvoltBatterie_id = hochvoltBatterie_id;
    }

    public  HochVoltBatterie() {}

    public int getGewicht() {
        return gewicht;
    }

    public void setGewicht(int gewicht) {
        this.gewicht = gewicht;
    }

    public String getProduktionsJahr() {
        return produktionsJahr;
    }

    public void setProduktionsJahr(String produktionsJahr) {
        this.produktionsJahr = produktionsJahr;
    }

    public String getStateOfHealth() {
        return stateOfHealth;
    }

    public void setStateOfHealth(String stateOfHealth) {
        this.stateOfHealth = stateOfHealth;
    }


    @Override
    public String toString() {
        return "HochVoltBatterie{" +
                "gewicht=" + gewicht +
                ", produktionsJahr='" + produktionsJahr + '\'' +
                ", stateOfHealth='" + stateOfHealth + '\'' +
                ", credit=" + credit +
                ", hochvoltBatterie_id=" + hochvoltBatterie_id +
                '}';
    }
//    public Energiespeicher getEnergiespeicher() {
//        return energiespeicher;
//    }
//
//    public void setEnergiespeicher(Energiespeicher energiespeicher) {
//        this.energiespeicher = energiespeicher;
//    }
}

