package Code;

public class Vehicle {
    private Elektromotor elektromotor = new Elektromotor();
    private Energiespeicher energiespeicher = new Energiespeicher();
    private Fahrwerkkomponente fahrwerkkomponente = new Fahrwerkkomponente();
    private int vehicle_id;
    private int credit;
    private int onwer_id=-1;


    public int getOnwer_id() {
        return onwer_id;
    }

    public void setOnwer_id(int onwer_id) {
        this.onwer_id = onwer_id;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public Elektromotor getElektromotor() {
        return elektromotor;
    }

    public void setElektromotor(Elektromotor elektromotor) {
        this.elektromotor = elektromotor;
    }

    public Energiespeicher getEnergiespeicher() {
        return energiespeicher;
    }

    public void setEnergiespeicher(Energiespeicher energiespeicher) {
        this.energiespeicher = energiespeicher;
    }

    public Fahrwerkkomponente getFahrwerkkomponente() {
        return fahrwerkkomponente;
    }

    public void setFahrwerkkomponente(Fahrwerkkomponente fahrwerkkomponente) {
        this.fahrwerkkomponente = fahrwerkkomponente;
    }

    public int getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(int vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "elektromotor=" + elektromotor +
                ", energiespeicher=" + energiespeicher +
                ", fahrwerkkomponente=" + fahrwerkkomponente +
                ", vehicle_id=" + vehicle_id +
                ", credit=" + credit +
                ", onwer_id=" + onwer_id +
                '}';
    }
}

