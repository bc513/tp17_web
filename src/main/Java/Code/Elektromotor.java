package Code;

public class Elektromotor {
    
    private int credit = -1;		//## attribute credit

    private String name;		//## attribute name

    private int elektromotroe_id;
    

    public  Elektromotor() {
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getElektromotroe_id() {
        return elektromotroe_id;
    }

    public void setElektromotroe_id(int elektromotroe_id) {
        this.elektromotroe_id = elektromotroe_id;
    }

    @Override
    public String toString() {
        return "Elektromotor{" +
                "credit=" + credit +
                ", name='" + name + '\'' +
                ", elektromotroe_id=" + elektromotroe_id +
                '}';
    }
}


