package de.ostfalia.tp;

import Client.Client;
import Client.WritePassword;
import Code.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/passwordVerify")
public class PasswordVerify extends HttpServlet {
    User user = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        System.out.println(cookies);
        String username = "username";
        for (Cookie cookie : cookies
        ) {
            if (cookie.getName().equals("username")) {
                username = cookie.getValue();
                System.out.println("get Cookie");
                System.out.println(cookie.getValue());
            }
        }
        System.out.println(username);
        List<User> ulist = (List<User>) request.getSession().getAttribute("Userlist");
        int user_index = -1;
        for (User u : ulist
        ) {
            user_index++;
            if (u.getUsername().equals(username)) {
                this.user=u;
                System.out.println("found user");
                System.out.println(u.getUsername());
            }
        }
        String alt_password = request.getParameter("alt_password");
        String neu_password = request.getParameter("neu_password");
        System.out.println(request.getParameter("alt_password"));
        System.out.println("user_password  " + user.getPassword());
        if (user.getPassword().equals(request.getParameter("alt_password"))) {
            user.setPassword(request.getParameter("neu_password"));
            ulist.set(user_index,this.user);
            request.getSession().setAttribute("Userlist", ulist);
            request.setAttribute("password_info", "success changed");
            System.out.println(user.getPassword());
            WritePassword writePassword = null;
            try {
                writePassword = new WritePassword(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                new Client(writePassword).run();
            } catch (Exception e) {
                e.printStackTrace();
            }
            request.getRequestDispatcher("/password.jsp").forward(request, response);
        } else {
            request.setAttribute("password_info", "failed changed");
            request.getRequestDispatcher("/password.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
