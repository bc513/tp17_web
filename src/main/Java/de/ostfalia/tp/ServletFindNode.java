package de.ostfalia.tp;

import Client.BrowseVehicle;
import Client.FindVehicle;
import Code.Vehicle;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/servletFindNode")
public class ServletFindNode extends HttpServlet {
    String kost = "223";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s =request.getParameter("id").toString();
        FindVehicle f = new FindVehicle();

        Vehicle vehicle= new Vehicle();
        Map<String, NodeId> map=null;
        try {
             map= new BrowseVehicle().getMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        f.setMap(map);

        try {
            vehicle = f.getFind(map);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (vehicle.getVehicle_id()==Integer.parseInt(s)){
            request.setAttribute("vehicle",vehicle);
            request.setAttribute("kost",kost);
            System.out.println("get vehicle");
        }else{
            request.setAttribute("kost","-1");
        }

        System.out.println(kost+" in serverFindnode");
        request.getRequestDispatcher("/userschnitte.jsp").forward(request,response);
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
