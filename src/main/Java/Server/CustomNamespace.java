//package Server;
//
//import Code.UserManager;
//
//import org.eclipse.milo.opcua.sdk.core.AccessLevel;
//import org.eclipse.milo.opcua.sdk.core.Reference;
//import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
//import org.eclipse.milo.opcua.sdk.server.api.DataItem;
//import org.eclipse.milo.opcua.sdk.server.api.ManagedNamespace;
//import org.eclipse.milo.opcua.sdk.server.api.MonitoredItem;
//import org.eclipse.milo.opcua.sdk.server.model.nodes.objects.BaseEventNode;
//import org.eclipse.milo.opcua.sdk.server.model.nodes.objects.ServerNode;
//import org.eclipse.milo.opcua.sdk.server.nodes.UaFolderNode;
//import org.eclipse.milo.opcua.sdk.server.nodes.UaNode;
//import org.eclipse.milo.opcua.sdk.server.nodes.UaNodeContext;
//import org.eclipse.milo.opcua.sdk.server.nodes.UaVariableNode;
//import org.eclipse.milo.opcua.sdk.server.util.SubscriptionModel;
//import org.eclipse.milo.opcua.stack.core.Identifiers;
//import org.eclipse.milo.opcua.stack.core.types.builtin.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.List;
//import java.util.UUID;
//import java.util.concurrent.TimeUnit;
//
//import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ubyte;
//import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ushort;
//
//public class CustomNamespace extends ManagedNamespace {
//    public static final String URI = "urn:my:custom:namespace";
//
//    private Logger logger = LoggerFactory.getLogger(getClass());
//
//    private  final SubscriptionModel subscriptionModel;
//
//    public CustomNamespace(final OpcUaServer server, final String uri){
//        super(server,uri);
//        this.subscriptionModel = new SubscriptionModel(server,this);
//        onStartup();
//    }
//
//    private void registerItems(final UaNodeContext context) {
//        NodeId folderNodeId1 = newNodeId("Vehicle");
//        NodeId folderNodeId2 = newNodeId("User");
//        final UaFolderNode folderNode1 = new UaFolderNode(
//                getNodeContext(),
//                folderNodeId1,
//                newQualifiedName("Vehicle"),
//                LocalizedText.english("Vehicle"));
//        final UaFolderNode folderNode2 = new UaFolderNode(
//                getNodeContext(),
//                folderNodeId2,
//                newQualifiedName("User"),
//                LocalizedText.english("User"));
//
//        context.getNodeManager().addNode(folderNode1);
//        context.getNodeManager().addNode(folderNode2);
//
//        String name = "Vehicle_ID";
//        UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name))
//                .setDisplayName(LocalizedText.english(name))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList().get(0).getVehicle_id())));
//
//        getNodeManager().addNode(node);
//        folderNode1.addOrganizes(node);
//
//        folderNode1.addReference(new Reference(
//                folderNode1.getNodeId(),
//                Identifiers.Organizes,
//                Identifiers.ObjectsFolder.expanded(), false
//        ));
//        folderNode2.addReference(new Reference(
//                folderNode2.getNodeId(),
//                Identifiers.Organizes,
//                Identifiers.ObjectsFolder.expanded(), false
//        ));
//
//        addVariableNodes(folderNode1);
//
//        addVariableNodes2(folderNode2);
//
//        UaNode serverNode = getServer()
//                .getAddressSpaceManager()
//                .getManagedNode(Identifiers.Server)
//                .orElse(null);
//
//        if (serverNode instanceof ServerNode) {
//            ((ServerNode) serverNode).setEventNotifier(ubyte(1));
//
//            // Post a bogus Event every couple seconds
//            getServer().getScheduledExecutorService().scheduleAtFixedRate(() -> {
//                try {
//                    BaseEventNode eventNode = getServer().getEventFactory().createEvent(
//                            newNodeId(UUID.randomUUID()),
//                            Identifiers.BaseEventType
//                    );
//
//                    eventNode.setBrowseName(new QualifiedName(1, "foo"));
//                    eventNode.setDisplayName(LocalizedText.english("foo"));
//                    eventNode.setEventId(ByteString.of(new byte[]{0, 1, 2, 3}));
//                    eventNode.setEventType(Identifiers.BaseEventType);
//                    eventNode.setSourceNode(serverNode.getNodeId());
//                    eventNode.setSourceName(serverNode.getDisplayName().getText());
//                    eventNode.setTime(DateTime.now());
//                    eventNode.setReceiveTime(DateTime.NULL_VALUE);
//                    eventNode.setMessage(LocalizedText.english("event message!"));
//                    eventNode.setSeverity(ushort(2));
//
//                    getServer().getEventBus().post(eventNode);
//
//                    eventNode.delete();
//                } catch (Throwable e) {
//                    logger.error("Error creating EventNode: {}", e.getMessage(), e);
//                }
//            }, 0, 2, TimeUnit.SECONDS);
//        }
//
//    }
//
//    private void addVariableNodes(UaFolderNode rootNode){
//        addEnergiespeicherNodes(rootNode);
//        addElektromotorNodes(rootNode);
//        addFahrzeugenkomponentenNodes(rootNode);
//    }
//    private void addVariableNodes2(UaFolderNode rootNode){
//        String name = "User_ID";
//        UaVariableNode node1 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name))
//                .setDisplayName(LocalizedText.english(name))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node1.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getId())));
//
//        getNodeManager().addNode(node1);
//        rootNode.addOrganizes(node1);
//
//        String name2 = "User_Name";
//        UaVariableNode node2 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name2))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name2))
//                .setDisplayName(LocalizedText.english(name2))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//       node2.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getUsername())));
//
//        getNodeManager().addNode(node2);
//        rootNode.addOrganizes(node2);
//
//        String name3 = "Name";
//        UaVariableNode node3 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name3))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name3))
//                .setDisplayName(LocalizedText.english(name3))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node3.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getName())));
//
//        getNodeManager().addNode(node3);
//        rootNode.addOrganizes(node3);
//
//        String name4 = "Birthday";
//        UaVariableNode node4 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name4))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name4))
//                .setDisplayName(LocalizedText.english(name4))
//                .setDataType(Identifiers.String)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node4.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getGeburtstag().toString())));
//
//        getNodeManager().addNode(node4);
//        rootNode.addOrganizes(node4);
//
//        String name5 = "Password";
//        UaVariableNode node5 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name5))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name5))
//                .setDisplayName(LocalizedText.english(name5))
//                .setDataType(Identifiers.String)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node5.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getPassword())));
//
//        getNodeManager().addNode(node5);
//        rootNode.addOrganizes(node5);
//        String name6 = "Credit";
//        UaVariableNode node6 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name6))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name6))
//                .setDisplayName(LocalizedText.english(name6))
//                .setDataType(Identifiers.Float)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node6.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getCredit())));
//
//        getNodeManager().addNode(node6);
//        rootNode.addOrganizes(node6);
//        String name7 = "Email";
//        UaVariableNode node7 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name7))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name7))
//                .setDisplayName(LocalizedText.english(name7))
//                .setDataType(Identifiers.String)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node7.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getEmail())));
//
//        getNodeManager().addNode(node7);
//        rootNode.addOrganizes(node7);
//
//        String name8 = "Usertype";
//        UaVariableNode node8 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name8))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name8))
//                .setDisplayName(LocalizedText.english(name8))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node8.setValue(new DataValue(new Variant(UserManager.getInstance().getUser().get(0).getUsertype())));
//
//        getNodeManager().addNode(node8);
//        rootNode.addOrganizes(node8);
//    }
//
//
//
//
//    private void addElektromotorNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("Elektromotor"),
//                newQualifiedName("Elektromotor"),
//                LocalizedText.english("Elektromotor")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        rootNode.addOrganizes(adminFolder);
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//        String name1 = "Elektromotor_ID";
//        UaVariableNode node1 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name1))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name1))
//                .setDisplayName(LocalizedText.english(name1))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node1.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList().get(0).getElektromotor_id())));
//
//        getNodeManager().addNode(node1);
//        adminFolder.addOrganizes(node1);
//    }
//
//    private void addFahrzeugenkomponentenNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("Fahrzeugkomponente"),
//                newQualifiedName("Fahrzeugkomponente"),
//                LocalizedText.english("Fahrzeugkomponente")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        rootNode.addOrganizes(adminFolder);
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//        String name2 = "Fahrzeugkomponente_ID";
//        UaVariableNode node2 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name2))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name2))
//                .setDisplayName(LocalizedText.english(name2))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        node2.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList().get(0).getFahrzeugkomponente_id())));
//
//        getNodeManager().addNode(node2);
//        adminFolder.addOrganizes(node2);
//    }
//
//
//    private void addEnergiespeicherNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("Energiespeicher"),
//                newQualifiedName("Energiespeicher"),
//                LocalizedText.english("Energiespeicher")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        rootNode.addOrganizes(adminFolder);
//
//        addHochVoltBatterieNodes(adminFolder);
//
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//            String name = "Energiespeicher_ID";
//            UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                    .setNodeId(newNodeId(name))
//                    .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                    .setBrowseName(newQualifiedName(name))
//                    .setDisplayName(LocalizedText.english(name))
//                    .setDataType(Identifiers.Integer)
//                    .setTypeDefinition(Identifiers.BaseDataVariableType)
//                    .build();
//
//            node.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList().get(0).getEnergiespeicher_id())));
//
//            getNodeManager().addNode(node);
//            adminFolder.addOrganizes(node);
//
//            node.addReference(new Reference(
//                    node.getNodeId(),
//                    Identifiers.HasChild,
//                    adminFolder.getNodeId().expanded(),false
//            ));
//
//        }
//
//    private void addHochVoltBatterieNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("HochVoltBatterie"),
//                newQualifiedName("HochVoltBatterie"),
//                LocalizedText.english("HochVoltBatterie")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        rootNode.addOrganizes(adminFolder);
//
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//        String name = "HochVoltBatterie_ID";
//        UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name))
//                .setDisplayName(LocalizedText.english(name))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//       // node.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList())));
//
//        getNodeManager().addNode(node);
//        adminFolder.addOrganizes(node);
//
//        addStateOfHealthNodes(adminFolder);
//        addGewichtNodes(adminFolder);
//        addProduktionsJahrNodes(adminFolder);
//
//        node.addReference(new Reference(
//                node.getNodeId(),
//                Identifiers.HasChild,
//                adminFolder.getNodeId().expanded(),false
//        ));
//
//    }
//
//    private void addStateOfHealthNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("StateOfHealth"),
//                newQualifiedName("StateOfHealth"),
//                LocalizedText.english("StateOfHealth")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//        String name = "HochVoltBatterie_ID";
//        UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name))
//                .setDisplayName(LocalizedText.english(name))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        // node.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList())));
//
//        getNodeManager().addNode(node);
//        adminFolder.addOrganizes(node);
//
//        node.addReference(new Reference(
//                node.getNodeId(),
//                Identifiers.HasChild,
//                adminFolder.getNodeId().expanded(),false
//        ));
//    }
//
//    private void addGewichtNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("Gewicht"),
//                newQualifiedName("Gewicht"),
//                LocalizedText.english("Gewicht")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//        String name = "Gewicht_ID";
//        UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name))
//                .setDisplayName(LocalizedText.english(name))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        // node.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList())));
//
//        getNodeManager().addNode(node);
//        adminFolder.addOrganizes(node);
//
//        node.addReference(new Reference(
//                node.getNodeId(),
//                Identifiers.HasChild,
//                adminFolder.getNodeId().expanded(),false
//        ));
//    }
//
//    private void addProduktionsJahrNodes(UaFolderNode rootNode){
//        UaFolderNode adminFolder = new UaFolderNode(
//                getNodeContext(),
//                newNodeId("ProduktionsJahr"),
//                newQualifiedName("ProduktionsJahr"),
//                LocalizedText.english("ProduktionsJahr")
//        );
//
//        getNodeManager().addNode(adminFolder);
//        adminFolder.addReference(new Reference(
//                adminFolder.getNodeId(),
//                Identifiers.HasChild,
//                rootNode.getNodeId().expanded(),false
//        ));
//
//        String name = "ProduktionsJahr_ID";
//        UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
//                .setNodeId(newNodeId(name))
//                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
//                .setBrowseName(newQualifiedName(name))
//                .setDisplayName(LocalizedText.english(name))
//                .setDataType(Identifiers.Integer)
//                .setTypeDefinition(Identifiers.BaseDataVariableType)
//                .build();
//
//        // node.setValue(new DataValue(new Variant(VehicleManagement.getInstance().getList())));
//
//        getNodeManager().addNode(node);
//        adminFolder.addOrganizes(node);
//
//        node.addReference(new Reference(
//                node.getNodeId(),
//                Identifiers.HasChild,
//                adminFolder.getNodeId().expanded(),false
//        ));
//    }
//
//    @Override
//    protected void onStartup() {
//        super.onStartup();
//        registerItems(getNodeContext());
//    }
//
//    @Override
//    public void onDataItemsCreated(final List<DataItem> dataItems) {
//        this.subscriptionModel.onDataItemsCreated(dataItems);
//    }
//
//    @Override
//    public void onDataItemsModified(final List<DataItem> dataItems) {
//        this.subscriptionModel.onDataItemsModified(dataItems);
//    }
//
//    @Override
//    public void onDataItemsDeleted(final List<DataItem> dataItems) {
//        this.subscriptionModel.onDataItemsDeleted(dataItems);
//    }
//
//    @Override
//    public void onMonitoringModeChanged(final List<MonitoredItem> monitoredItems) {
//        this.subscriptionModel.onMonitoringModeChanged(monitoredItems);
//    }
//}
